---
title: "Terraform Datasync Module"
date: 2024-09-18T11:24:50-04:00
draft: false
---

Simplifying Hybrid Data Transfers and Backups with the New AWS DataSync Terraform Module

In September 2024 we published a new Terraform module to streamline the deployment and management of the AWS DataSync service. [AWS DataSync](https://aws.amazon.com/datasync/) is a highly scalable and secure data transfer service that enables seamless movement of data between on-premises storage and various AWS storage services. The new Terraform module provides three pre-built examples. Some features of V1:
- Seamless configuration of DataSync Locations, Tasks, and associated IAM roles
- Support for cross-account data transfers between S3 buckets
- Example configurations for common hybrid scenarios like EFS to S3 and S3 to S3
- Adherence to Terraform best practices for maintainable infrastructure as code

The DataSync Terraform module is available now in [Terraform module registry](https://registry.terraform.io/modules/aws-ia/datasync/aws/latest). To get started, check out the README and example configurations. 