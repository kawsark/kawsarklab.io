---
title: "Selected works"
date: 2020-09-15T11:30:03+00:00
weight: 1
# aliases: ["/first"]
tags: ["first", "projects"]
author: "Me"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: false
draft: false
hidemeta: false
comments: false
description: "My projects"
canonicalURL: "https://canonical.url/to/page"
disableHLJS: true # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: true
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: false
ShowWordCount: true
ShowRssButtonInSectionTermList: true
UseHugoToc: true
cover:
    image: "<image path/url>" # image path/url
    alt: "<alt text>" # alt text
    caption: "<text>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
editPost:
    URL: "https://github.com/<path_to_repo>/content"
    Text: "Suggest Changes" # edit text
    appendFilePath: true # to append file path to Edit link
---
## Selected projects and highlights

### Talks
- [DOP322 Re:Invent 2023 Builder's session: Accelerate Data Science coding with CodeWhisperer](https://aws.amazon.com/blogs/devops/your-devops-and-developer-productivity-guide-to-reinvent-2023/)
- [AWS NYC Meetup: Accelerating application upgrades with Amazon Q Code Transformation](https://www.meetup.com/aws-nyc/events/300431036/)

### OSS Code contributions
- [AWS Storage Gateway Terraform Module](https://registry.terraform.io/modules/aws-ia/storagegateway/aws/latest)
- [AWS DataSync Terraform Module](https://registry.terraform.io/modules/aws-ia/datasync/aws/latest)
- [Ansible AWX Vault AppRole Authentication Method plugin](https://github.com/kawsark/awx-approle-ansible)

### Writing
- [My posts on Medium](https://medium.com/@kawsaur)

#### AWS
  - [Automate Amazon S3 File Gateway on Amazon EC2 with Terraform by HashiCorp](https://aws.amazon.com/blogs/storage/automate-amazon-s3-file-gateway-on-amazon-ec2-with-terraform-by-hashicorp/)
  - [Enhance conversational AI with advanced routing techniques with Amazon Bedrock](https://aws.amazon.com/blogs/machine-learning/enhance-conversational-ai-with-advanced-routing-techniques-with-amazon-bedrock/)
  - [Streamline access to most used AWS services using VPC Endpoints](https://aws.amazon.com/blogs/networking-and-content-delivery/streamline-access-to-most-used-aws-services-using-vpc-endpoints/)

#### HashiCorp
- [Fully Automated SaaS-Based DevOps with Terraform Cloud and GitLab](https://www.hashicorp.com/resources/fully-saas-based-devops-with-terraform-cloud-and-gitlab)
- GitLab CI/CD, Terraform Cloud and HashiCorp Vault: [example repo](https://gitlab.com/gitlab-com/alliances/hashicorp/sandbox-projects/hashitalk-terraform-cloud-and-gitlab-demo) and [blog post](https://medium.com/hashicorp-engineering/secure-infrastructure-provisioning-with-terraform-cloud-vault-gitlab-ci-eaeabf7e31a1?source=friends_link&sk=84e7d1d902d5eb8d260e85de9f941e07)
- [Dynamic Azure Credentials for Applications and CI/CD Pipelines](https://www.hashicorp.com/resources/dynamic-azure-credentials-for-applications-and-ci-cd-pipelines)
- [Onboarding the Azure Secrets Engine for Vault](https://medium.com/hashicorp-engineering/onboarding-the-azure-secrets-engine-for-vault-f09d48c68b69)
- [TLS enabled Consul cluster using Terraform](https://github.com/kawsark/terraform-gcp-consul)
- Vault application onboarding
  - [HashiConf Eu Presentation](2021.06.09-HashiConfEu-vault-adoption-v1.pdf)
  - [Repo (Vault guides)](https://github.com/hashicorp/vault-guides/tree/master/operations/onboarding)
- ARCHIVED [A wrapper for Vault's transform secret engine](https://github.com/kawsark/transform.py)
- ARCHIVED Kittens-as-a-Service - [HashiCat application repo](https://github.com/kawsark/hashicat-docker), [blog post](https://medium.com/hashicorp-engineering/kittens-as-a-service-layer-7-traffic-management-security-with-consul-connect-f5965fac5aa?source=friends_link&sk=ad4b747d7f71889772569cb7f069b8ad)

This website is powered by [GitLab Pages](https://about.gitlab.com/features/pages/)
/ [Hugo](https://gohugo.io) and can be built in under 1 minute.
Literally. It uses the `beautifulhugo` theme.
